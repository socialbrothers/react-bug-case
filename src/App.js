import logo from './logo.svg';
import {SubmitButton} from './components/Button'
import './App.css';

import nl from './locales/nl.json'
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';


i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    nl: {
      translation: nl,
    },
    lang: 'nl',
    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>


        <SubmitButton />
    
      </header>
    </div>
  );
}

export default App;
