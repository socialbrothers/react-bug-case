import { useTranslation } from "react-i18next"

export const SubmitButton = () => {
    const { t } = useTranslation();

    return <button>{t('COMPONENTS.SUBMIT_BUTTON.LABEL')}</button>

}